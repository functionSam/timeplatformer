﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Controller2D : RaycastController
{

    // public CollisionInfo collisions;

    [HideInInspector]
    public Vector2 playerInput;

    public override void Start()
    {
        base.Start();

    }

    public void Move(Vector3 velocity, Vector2 input = new Vector2(), bool standingOnPlatform = false)
    {

        UpdateRaycastOrigins();

        collisions.Reset();

        playerInput = input;

        if (velocity.x != 0)
        {

            collisions.faceDirection = (int)Mathf.Sign(velocity.x);

        }

        HorCollisions(ref velocity);

        if (velocity.y != 0)
        {
            VertCollisions(ref velocity);
        }

        transform.Translate(velocity);

        if (standingOnPlatform)
        {

            collisions.below = true;

        }
        

    }

    /// <summary>
    /// Horizontal Collisions
    /// </summary>
    /// <param name="velocity"></param>
    void HorCollisions(ref Vector3 velocity)
    {
        float directionX = collisions.faceDirection;
        float rayLength = Mathf.Abs(velocity.x) + skinWidth;

        if (Mathf.Abs(velocity.x) < skinWidth)
        {
            rayLength = 2 * skinWidth;
        }

        for (int i = 0; i < horRayCount; i++)
        {
            Vector2 rayOrigin = (directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;
            rayOrigin += Vector2.up * (horRaySpacing * i);
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

            Debug.DrawRay(rayOrigin, Vector2.right * directionX * rayLength, Color.green);

            if (hit)
            {
                if (hit.distance == 0)
                {
                    continue;
                }

                velocity.x = (hit.distance - skinWidth) * directionX;
                rayLength = hit.distance;
                collisions.left = directionX == -1;
                collisions.right = directionX == 1;
            }
        }
    }

    /// <summary>
    /// Vertical Collisions
    /// </summary>
    /// <param name="velocity"></param>
    void VertCollisions(ref Vector3 velocity)
    {
        float directionY = Mathf.Sign(velocity.y);
        float rayLength = Mathf.Abs(velocity.y) + skinWidth;

        for (int i = 0; i < vertRayCount; i++)
        {
            Vector2 rayOrigin = (directionY == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
            rayOrigin += Vector2.right * (vertRaySpacing * i + velocity.x);
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, collisionMask);

            Debug.DrawRay(rayOrigin, Vector2.up * directionY * rayLength, Color.green);

            if (hit)
            {

                velocity.y = (hit.distance - skinWidth) * directionY;
                rayLength = hit.distance;

                collisions.below = directionY == -1;
                collisions.above = directionY == 1;

            }
        }
    }
}
