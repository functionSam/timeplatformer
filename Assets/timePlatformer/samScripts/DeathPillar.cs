﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// MonoBehaviour

public class DeathPillar : RaycastController
{
    #region Old death pillar
    /*
    [SerializeField]
    float closedLimit = 0.3f;
    [SerializeField]
    GameObject DeathZone;

    bool isClosed = false;

    Vector2 startPosition;
    Vector2 endPosition;

    private float distance = 10f;

    // Use this for initialization
    void Start()
    {
        startPosition = transform.position;
        endPosition = startPosition + Vector2.down * distance;


    }

    // Update is called once per frame
    void Update()
    {
        Move(Mathf.Abs(Mathf.Sin(Time.time)));
    }

    void Move(float _time)
    {
        transform.position = Vector2.Lerp(startPosition, endPosition, _time);

        if (!isClosed && _time > closedLimit)
        {
            isClosed = true;
            DeathZone.SetActive(true);
            GetComponent<SpriteRenderer>().color = Color.red;
        }
        else if (isClosed && _time < closedLimit)
        {
            isClosed = false;
            DeathZone.SetActive(false);
            GetComponent<SpriteRenderer>().color = Color.white;
        }
    }
    */
    #endregion

    public Vector3 move;

    public LayerMask passengerMask;

    public override void Start ()
    {
        base.Start();
    }

    void Update()
    {
        UpdateRaycastOrigins();
        Vector3 velocity = move * Time.deltaTime;

        MoveDeathPillar(velocity);
        transform.Translate(velocity);
    }
    
    void MoveDeathPillar(Vector3 velocity)
    {
        // HashSet<Transform> movedPassengers = new HashSet<Transform>();
        HashSet<Transform> movedDeathPillar = new HashSet<Transform>();
        float directionY = Mathf.Sign(velocity.y);

        //Vertically moving pillar of death.
        if (velocity.y != 0)
        {
            float rayLength = Mathf.Abs(velocity.y) + skinWidth;

            for (int i = 0; i < vertRayCount; i++)
            {
                Vector2 rayOrigin = (directionY == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
                rayOrigin += Vector2.right * (vertRaySpacing * i);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, collisionMask);
                // RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, passengerMask);

                if (hit)
                {
                    if (!movedDeathPillar.Contains(hit.transform))
                    {
                        movedDeathPillar.Add(hit.transform);
                        //float pushX = (directionY == 1) ? velocity.x : 0;
                        float pushY = velocity.y - (hit.distance - skinWidth) * directionY;

                        hit.transform.Translate(new Vector3(0, pushY));
                    }
                }
            }
        }
    }
}
