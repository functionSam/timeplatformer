﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathZone : MonoBehaviour
{

    // Might need RigidBody2D?
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    void OnDrawGizmos()
    {
        // Would be nice to draw a box that can only be seen in the scene view.
        Gizmos.color = new Color(1, 0.3f, 0.3f, 0.5f);
        //Gizmos.DrawCube(focusArea.centre, focusAreaSize);
        Gizmos.DrawCube(transform.position, transform.lossyScale);
    }

    void OnTriggerEnter2D (Collider2D _collider)
    {
        Debug.Log(_collider);
        if (_collider.gameObject.tag == "Player")
        {

            Debug.Log("Player is dead");
            GameManager.Reset();
        }

    }

}
