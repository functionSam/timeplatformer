﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    List<IResetable> resetables;

    static GameManager instance;
    [SerializeField, Tooltip("Allow player to control time.")]
    bool allowTimeControl = true;
    public static bool AllowTimeControl { get { return instance.allowTimeControl; } }

    public delegate void OnReset();
    public static event OnReset OnResetE;

	void Awake ()
    {
        instance = this;
        resetables = new List<IResetable>();
	}
	
    public static void Add (IResetable resetable)
    {
        instance.resetables.Add(resetable);
    }

    public void SetAllowTimeControl(bool allow)
    {
        allowTimeControl = allow;
    }

    public static void Reset ()
    {
        for (int i = 0; i < instance.resetables.Count; i++)
        {
            instance.resetables[i].Reset();
        }

        if (OnResetE != null)
        {
            OnResetE();
        }
    }
}
