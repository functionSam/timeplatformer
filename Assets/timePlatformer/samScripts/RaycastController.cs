﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class RaycastController : MonoBehaviour
{
    public LayerMask collisionMask;

    public const float skinWidth = 0.015f;

    // Raycast stuff \\

    // Horizontal RayCount
    public int horRayCount = 4;
    // Vertical RayCount
    public int vertRayCount = 4;

    // Horizontal RaySpacing
    [HideInInspector]
    public float horRaySpacing;
    // Vertical RatSpacing
    [HideInInspector]
    public float vertRaySpacing;

    public BoxCollider2D collider;
    public RaycastOrigins raycastOrigins;

    public CollisionInfo collisions;

    // Use this for initialization
    public virtual void Awake()
    {
        collisions.faceDirection = 1;
        collider = GetComponent<BoxCollider2D>();

    }

    public virtual void Start ()
    {

        CalculateRaySpacing();

    }

    public void UpdateRaycastOrigins()
    {
        Bounds bounds = collider.bounds;
        bounds.Expand(skinWidth * -2);

        raycastOrigins.bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
        raycastOrigins.bottomRight = new Vector2(bounds.max.x, bounds.min.y);
        raycastOrigins.topLeft = new Vector2(bounds.min.x, bounds.max.y);
        raycastOrigins.topRight = new Vector2(bounds.max.x, bounds.max.y);
    }

    public void CalculateRaySpacing()
    {

        Bounds bounds = collider.bounds;
        bounds.Expand(skinWidth * -2);

        horRayCount = Mathf.Clamp(horRayCount, 2, int.MaxValue);
        vertRayCount = Mathf.Clamp(vertRayCount, 2, int.MaxValue);

        horRaySpacing = bounds.size.y / (horRayCount - 1);
        vertRaySpacing = bounds.size.y / (vertRayCount - 1);

    }

    public struct RaycastOrigins
    {
        public Vector2 topLeft, topRight;
        public Vector2 bottomLeft, bottomRight;

    }

    public struct CollisionInfo
    {
        public bool above, below;
        public bool left, right;

        public int faceDirection;

        public void Reset()
        {

            above = below = false;
            left = right = false;

        }

    }

}
