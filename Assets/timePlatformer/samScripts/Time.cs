﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Time 
{
    public static float deltaTime { get { return UnityEngine.Time.deltaTime * timeScale; } }
    public static float playerDeltaTime { get { return UnityEngine.Time.deltaTime * playerTimeScale; } }
    public static float time { get { return UnityEngine.Time.time; } }
    public static float realtimeSinceStartup { get { return UnityEngine.Time.realtimeSinceStartup; } }

    public static int frameCount { get { return UnityEngine.Time.frameCount; } }

    private static float timeScale = 1f;
    private static float playerTimeScale = 1f;

    /*
    public static void StopTime()
    {
        timeScale = 0f;
        // TODO: change Player time scale.
    }
    */

    public static void Slowmotion()
    {
        timeScale = 0.15f;
        playerTimeScale = 0.9f;
    }

    public static void NormalTime()
    {
        timeScale = 1f;
        playerTimeScale = 1f;
    }
}
