﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Pair this cript with the "Deathzone" prefab.

// Sprite's position needs "top left" or the child objects won't work properly.
[RequireComponent(typeof(SpriteRenderer))]

// Generates a nice set of repeated sprites inside a streched sprite renderer
public class SpriteRepeat : MonoBehaviour
{
    SpriteRenderer sprite;
    void Awake()
    {
        // Get the current sprite with an unscaled size
        sprite = GetComponent<SpriteRenderer>();
        Vector2 spriteSize = new Vector2(sprite.bounds.size.x / transform.localScale.x, sprite.bounds.size.y / transform.localScale.y);

        // Generate a child prefab of the sprite renderer
        GameObject childPrefab = new GameObject();
        SpriteRenderer childSprite = childPrefab.AddComponent<SpriteRenderer>();
        childPrefab.transform.position = transform.position;
        childSprite.sprite = sprite.sprite;
        GameObject child;

        for (int i = 1, l = (int)Mathf.Round(sprite.bounds.size.x); i < l; i++)
        {
            child = Instantiate(childPrefab) as GameObject;
            child.transform.position = transform.position + (new Vector3(spriteSize.x, 0, 0) * i);
            child.transform.parent = transform;
        }




        // Loop through and spit out repeated tiles
        /*
        for (int i = 1, l = (int)Mathf.Round(sprite.bounds.size.x); i < l; i++)
        {
            child = Instantiate(childPrefab) as GameObject;
            child.transform.position = transform.position + (new Vector3(spriteSize.x, 0, 0) * i);
            child.transform.parent = transform;
        }
        */
        // Set the parent last on the prefab to prevent transform displacement
        childPrefab.transform.parent = transform;

        // Disable the currently existing sprite component since its now a repeated image
        sprite.enabled = false;
    }
}
