﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    [SerializeField]
    SpriteRenderer Flag;

    void OnTriggerEnter2D(Collider2D _collider)
    {
        Debug.Log(_collider);
        if (_collider.gameObject.tag == "Player")
        {

            Player player = _collider.GetComponent<Player>();
            player.NewCheckpoint(transform.position);
            RaiseFlag();
            Debug.Log("Checkpoint triggered");
            
        }

    }

    void RaiseFlag()
    {

        Flag.color = Color.green;

    }

}
