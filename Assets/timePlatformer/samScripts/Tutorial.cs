﻿using UnityEngine;
using UnityEngine.Events;

public class Tutorial : MonoBehaviour
{
    [SerializeField]
    UnityEvent onLevelReset;
   
	// Use this for initialization
	void Start ()
    {
        GameManager.OnResetE += OnReset;   		
	}

    private void OnReset()
    {
        onLevelReset.Invoke();
    }
}
