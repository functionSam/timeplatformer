﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    // Member Variables
    //bool left;
    //bool right;
    public float moveSpeed;
    public float jumpHeight;
    public float groundCheckRadius;
    //public float moveVelocity;

    private float moveVelocity;

    public Transform groundCheck;

    public LayerMask whatIsGround;

    private bool playerGrounded;

	// Use this for initialization
	void Start ()
    {
		


	}
	
    void FixedUpdate ()
    {

        playerGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);

    }

	// Update is called once per frame
	void Update ()
    {


        if (Input.GetButtonDown("Jump") && playerGrounded)
        {

            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, jumpHeight);

        }

        moveVelocity = moveSpeed * Input.GetAxisRaw("Horizontal");
        
        GetComponent<Rigidbody2D>().velocity = new Vector2(moveVelocity, GetComponent<Rigidbody2D>().velocity.y);

    }
}
