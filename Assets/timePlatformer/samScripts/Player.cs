﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Controller2D))]
public class Player : MonoBehaviour, IResetable
{
    
    // How high can the Player jump?
    public float jumpHeight = 4;
    // How fast will it take for the Player to reach the "top" of it's jump(?)
    public float timeToJumpApex = 0.4f;

    // How fast will the Player slide down the wall?
    public float wallSlideSpeedMax = 3;
    // The amount of time the Player will stick to a wall.
    public float wallStickTime = 0.25f;
    // So the Player can "unstick" from a wall.
    float timeToWallUnstick;

    // How fast can the PLayer move in the air(?)
    float accelTimeAirborne = 0.2f;
    float accelTimeGrounded = 0.1f;
    // How fast can the PLayer move?
    float moveSpeed = 6;

    [SerializeField]
    float runSpeed = 1;

    // Gravity in the game.
    float gravity = -20; 
    // The speed the Player can jump(?)
    float jumpVelocity = 8; 
    // Smooth movement along the X-axis.
    float velocityXSmooth;

    [SerializeField]
    float runSlowDownTime = 0.1f;
    float runSlowTimer = 0f;

    // A technique to jump upwards on a vertical wall.
    public Vector2 wallJumpClimb;
    // How high can the Player jump when they're on a wall.
    public Vector2 wallJumpOff;
    // How far can the PLayer jump across from a wall?
    public Vector2 wallLeap;
    Vector2 startPosition;

    Vector3 velocity;

    Controller2D controller;

	// Use this for initialization
	void Start ()
    {
        Register();
        startPosition = transform.position;
        controller = GetComponent<Controller2D>();

        gravity = -(2 * jumpHeight) / Mathf.Pow(timeToJumpApex, 2);
        jumpVelocity = Mathf.Abs(gravity * timeToJumpApex);
        // Print out the Player Gravity and Jump Velocity.
        print("Gravity: " + gravity + " Jump Velocity: " + jumpVelocity);
	}
	
	// Update is called once per frame
	void Update ()
    {
        

        Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        int wallDirectionX = (controller.collisions.left) ? -1 : 1;

        float targetVelocityX = input.x * moveSpeed * runSpeed;
        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmooth, (controller.collisions.below) ? accelTimeGrounded : accelTimeAirborne);

        bool wallSliding = false;
        if ((controller.collisions.left || controller.collisions.right) && !controller.collisions.below && velocity.y < 0 )
        {

            wallSliding = true;

            if (velocity.y < -wallSlideSpeedMax)
            {

                velocity.y = -wallSlideSpeedMax;

            }

            if (timeToWallUnstick > 0)
            {
                velocityXSmooth = 0;
                velocity.x = 0;

                if (input.x != wallDirectionX && input.x != 0)
                { 
                    timeToWallUnstick -= Time.playerDeltaTime;
                }
                else
                {

                    timeToWallUnstick = wallStickTime;

                }
            }
            else
            {

                timeToWallUnstick = wallStickTime;

            }

        }

        if (controller.collisions.above || controller.collisions.below)
        {

            velocity.y = 0;

        }

        // Jumping
        if (Input.GetButtonDown("Jump"))
        {
            // If the Character is sliding on a wall.
            if (wallSliding)
            {

                if (wallDirectionX == input.x)
                {

                    velocity.x = -wallDirectionX * wallJumpClimb.x;
                    velocity.y = wallJumpClimb.y;

                }
                else if (input.x == 0)
                {

                    velocity.x = -wallDirectionX * wallJumpOff.x;
                    velocity.y = wallJumpOff.y;

                }
                else 
                {

                    velocity.x = -wallDirectionX * wallLeap.x;
                    velocity.y = wallLeap.y;

                }

            }
            if (controller.collisions.below)
            {

                velocity.y = jumpVelocity;

            }


        }


        // The Player can run by holding the B-button or by holding the Shift key.
        if (Input.GetAxisRaw("Running") > 0.2f)
        {
            // Debug.Log(runSpeed);
            runSpeed = 2;

            runSlowTimer = 0f;
        }
        else 
        {
            
            if (runSlowTimer < runSlowDownTime)
                runSlowTimer += Time.playerDeltaTime;
            else
                runSpeed = 1;

        }


        velocity.y += gravity * Time.playerDeltaTime;
        controller.Move(velocity * Time.playerDeltaTime, input);

	}

    public void NewCheckpoint(Vector3 position)
    {
        startPosition = position;
    }

    public void Reset()
    {
        transform.position = startPosition;
        velocity.Set(0,0,0);
    }

    public void Register()
    {
        GameManager.Add(this);
    }
}
