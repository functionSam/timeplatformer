﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TimeController : MonoBehaviour
{
    [SerializeField]
    float maxTimeCharge = 10;
    float timeCharge = 10;

    [SerializeField]
    UnityEngine.UI.Image timeChargeUI, slowmotionBackround;

    // Also remove this if too much
    [SerializeField]
    TrailRenderer timeTrail;
    // Use this for initialization
    void Start ()
    {

    }
	
	// Update is called once per frame
	void Update ()
    {
        timeChargeUI.transform.parent.gameObject.SetActive(GameManager.AllowTimeControl);

        SetSlowAlpha(0);
        timeChargeUI.fillAmount = timeCharge / maxTimeCharge;
        if(GameManager.AllowTimeControl)
        {
            UpdateTimeControl();
        }
    }

    void UpdateTimeControl()
    {
        
        if (Input.GetAxisRaw("TimeTrigger") > 0)
        {
            if (timeCharge > Time.playerDeltaTime)
            {
                timeCharge -= Time.playerDeltaTime;
                Time.Slowmotion();
                SetSlowAlpha(0.5f);
            }
            else
            {
                Time.NormalTime();
                
            }

        }
        else
        {
            // Normaltime and recovered timeCharge
            timeCharge = Mathf.Min(maxTimeCharge, timeCharge + Time.playerDeltaTime * 2);

            Time.NormalTime();
            
        }
    }
    void SetSlowAlpha(float alpha)
    {
        Color c = slowmotionBackround.color;
        c.a = alpha;
        slowmotionBackround.color = c;

        // Remove this if it's too much
        // timeTrail.enabled = alpha > 0;
        
    }

}
