﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour
{
    //[SerializeField]
    //Scene changeLevel;
    public string changeLevel;

    public void OnTriggerEnter2D(Collider2D _collider)
    {
        if((_collider.gameObject.tag == "Player"))
        {
            SceneManager.LoadScene(changeLevel);
        }
    }

}
